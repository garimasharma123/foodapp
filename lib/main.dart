import 'package:flutter/material.dart';
import './screens/filters_screen.dart';
import './screens/tabs_screen.dart';
import './screens/meal_detail_screen.dart';
import './screens/category_meals_screen.dart';
import './screens/categories_screen.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatefulWidget {
  const MyApp({super.key});

  @override
  State<MyApp> createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        title: 'DesiMeals',
        theme: ThemeData(
            primarySwatch: Colors.blue, fontFamily: 'Schuyler-Italic'),
        // home: const CategoriesScreen(),
        routes: {
          '/': (ctx) => const TabsScreen(),
          CategoryMealsScreen.routeName: (ctx) => const CategoryMealsScreen(),
          MealDetailScreen.routeName: (ctx) => const MealDetailScreen(),
          FiltersScreen.routeName: (ctx) => const FiltersScreen(),
        },
        onGenerateRoute: (settings) {
          // print(settings.arguments);
          //return MaterialPageRoute(builder:(ctx) => const CategoriesScreen(),);
        },
        onUnknownRoute: (settings) {
          return MaterialPageRoute(
            builder: (ctx) => const CategoriesScreen(),
          );
        });
  }
}

/*
class MyHomePage extends StatefulWidget {
  const MyHomePage({super.key, required this.title});
  final String title;

  @override
  State<MyHomePage> createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
          child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: const <Widget>[
                Text(
                  'Navigation Time',
                ),
              ]
          )
      ),
    );
  }
}*/
