import 'package:flutter/material.dart';
import '../widgets/main_drawer.dart';

class FiltersScreen extends StatefulWidget {
  const FiltersScreen({Key? key}) : super(key: key);
  static const routeName = '/filters';

  @override
  State<FiltersScreen> createState() => _FiltersScreenState();
}

class _FiltersScreenState extends State<FiltersScreen> {
  bool _glutenFree = false;
  bool _vegetarian = false;
  bool _vegan = false;
  bool _lactoseFree = false;

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      appBar: AppBar(title: const Text('Your Filters'),),
      drawer: const MainDrawer(),
      body: Column(children:<Widget> [
        Container(
          padding: const EdgeInsets.all(20),
          child: Text(
              'Adjust your meal selection.',
            style: Theme.of(context).textTheme.titleMedium,
          ),
        ),
        Expanded(
            child:
            ListView(
              children:<Widget> [
                SwitchListTile(
                  title: const Text('Gluten-free'),
                  value: _glutenFree,
                  subtitle:const Text('Only include gluten-free meals.'),
                onChanged: (newValue){
                    setState(() {
                      _glutenFree = newValue;
                    });
                },
                ),
                SwitchListTile(
                  title: const Text('Vegetarian'),
                  value: _vegetarian,
                  subtitle:const Text('Only include vegetarian meals.'),
                  onChanged: (newValue){
                    setState(() {
                      _vegetarian = newValue;
                    });
                  },
                ),
                SwitchListTile(
                  title: const Text('Vegan'),
                  value: _vegan,
                  subtitle:const Text('Only include vegan meals.'),
                  onChanged: (newValue){
                    setState(() {
                      _vegan = newValue;
                    });
                  },
                ),
                SwitchListTile(
                  title: const Text('Lactose-free'),
                  value: _lactoseFree,
                  subtitle:const Text('Only include lactose-free meals.'),
                  onChanged: (newValue){
                    setState(() {
                      _lactoseFree = newValue;
                    });
                  },
                ),
              ],),
        ),
      ],),
    );
  }
}
